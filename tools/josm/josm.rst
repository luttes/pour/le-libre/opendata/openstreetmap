.. index::
   ! Josm

.. _josm:

===========================================
**Josm**
===========================================


.. _ref_josm_2023_09_04:

2023-09-04 **Nicolas Moyroud a partagé sous licence libre Creative Commons BY-SA un support de formation à l'éditeur de données OpenStreetMap JOSM** (Jean-Christophe Becquet)
==================================================================================================================================================================================

- :ref:`osm_2023:josm_2023_09_04`

Bonjour,

Nicolas Moyroud a partagé sous licence libre Creative Commons BY-SA un
support de formation à l'éditeur de données OpenStreetMap JOSM.

https://www.osmlab.fr/JOSM/OpenStreetMap_contribution_avec_JOSM.zip

Il y a 3 parties :

 - initiation
 - perfectionnement
 - intégration de données externes

Intéressant d'autant que les ressources pédagogiques consacrées à
ce logiciel très puissant de contribution à OpenStreetMap sont plutôt rares.

Voir aussi sur Dessinetaville :

Et si on bougeait en VTT ? comment faire pour améliorer OpenStreetMap
avec JOSM
https://ml.apitux.net/pipermail/dessinetaville/2013-April/000062.html

Le Butineur : Comment contribuer à OpenStreetMap ?
https://ml.apitux.net/pipermail/dessinetaville/2017-April/000279.html

Contribuer : quel outil pour OpenStreetMap ?
https://ml.apitux.net/pipermail/dessinetaville/2020-May/000634.html

JOSM fait son entrée dans le SILL (Socle Interministériel de Logiciels
Libres)
https://ml.apitux.net/pipermail/dessinetaville/2020-July/000652.html

Vidéo : 4 outils pour se lancer sur OpenStreetMap et dessiner son bout
du monde
https://ml.apitux.net/pipermail/dessinetaville/2020-October/000684.html

Vidéo : Tuto Extrême - JOSM en 3 minutes
https://ml.apitux.net/pipermail/dessinetaville/2020-November/000688.html

Compléter les adresses dans OpenStreetMap à partir des données Fantoir
avec JOSM
https://ml.apitux.net/pipermail/dessinetaville/2021-July/000781.html

JOSM - Importer facilement les données du Cadastre (bâtiments)
https://ml.apitux.net/pipermail/dessinetaville/2021-August/000782.html

Bonne journée

JCB
--
Merci de prendre en compte ma nouvelle adresse :
7 rue Charreton - 38000 Grenoble

Webinaire OpenStreetMap et services de mobilité - mardi 12 septembre
https://www.linkedin.com/posts/jean-christophe-becquet-b0131b210_openstreetmap-vercors-mobilitaez-activity-7102927766580248576-Eowk

Jean-Christophe Becquet - Expert conseil - APITUX
le logiciel libre et les données ouvertes au service des territoires
