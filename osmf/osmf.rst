.. index::
   ! La Fondation OpenStreetMap

.. _osmf:

===========================================
La Fondation OpenStreetMap (OSMF)
===========================================

- https://fr.wikipedia.org/wiki/Fondation_OpenStreetMap
- https://wiki.osmfoundation.org/wiki/About/fr
- :ref:`osm_2023:sotmfr_2023_09_06`
