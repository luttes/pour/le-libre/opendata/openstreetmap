.. index::
   ! Openstreetmap

.. raw:: html

    <a rel="me" href="https://qoto.org/@pvergain"></a>
    <a rel="me" href="https://framapiag.org/@pvergain"></a>


.. _openstreetmap:

===========================================
|osm| **Openstreetmap** |sotm|
===========================================

- https://wiki.openstreetmap.org/wiki/Main_Page
- https://www.openstreetmap.org
- https://fr.osm.social/@osm_fr
- https://mapstodon.space/@panoramax
- https://osmcal.org/events.rss
- https://peertube.openstreetmap.fr/videos/local
- https://peertube.openstreetmap.fr/feeds/videos.xml?sort=-publishedAt&isLocal=true
- https://weeklyosm.eu/
- https://www.weeklyosm.eu/en/feed
- https://mobilizon.openstreetmap.fr/


.. toctree::
   :maxdepth: 5

   licences/licenses
   tools/tools
   osmf/osmf
   rendus/rendus
